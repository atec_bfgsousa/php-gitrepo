<?php
namespace Git;
class Repo 
{
    public $id;
    public $name;
    public $url;
    public $description;

    function __construct(int $id,string $name,string $url,string $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->description = $description;
    }

}