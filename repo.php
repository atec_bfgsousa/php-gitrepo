<?php
$id = isset($_GET["id"]) ? intval($_GET["id"]) : die("Not sent id");
require __DIR__."/vendor/autoload.php";
use Git\Repo;
require("repos.php");
$index = -1;
for($i = 0; $i < sizeof($repos);$i++)
{
    if($repos[$i]->id == $id)
    {
        $index = $i;
    }
}
if($index == -1)
{
    die("Not found repo");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p><a href="./">Back</a></p>
    <p>Name : <a href="<?php echo $repos[$index]->url; ?>"><?php echo $repos[$index]->name; ?></a></p>
    <p>Description : <?php echo $repos[$index]->description; ?></p>
</body>
</html>