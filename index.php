<?php
require __DIR__."/vendor/autoload.php";
use Git\Repo;
require("repos.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Repos</h1>
    <?php foreach($repos as $repo) { ?>
    <p>Name : <a href="<?php echo $repo->url; ?>"><?php echo $repo->name; ?></a> <a href="repo.php?id=<?php echo $repo->id; ?>">View</a></p>
    <?php } ?>
</body>
</html>
